import React, { useEffect, useRef, useState } from 'react'

export default function App() {
  // count đếm ngược
  const [count, setCount] = useState(60)

  // dùng để handle trường hợp người dùng liên tục setInterval dẫn đến nhiều interval lồng nhau
  const [run, setRun] = useState(1)
  // get id interval
  const ref = useRef()
  const prevcurent = useRef()

  // đồng hồ sẽ được gọi lần đầu và trong trường hợp gọi nhiều lần bằng việc setRun, hàm clearup sẽ giúp đảm bảo chỉ duy nhất 1 interval hoạt động
  useEffect(() => {
    // get id
    ref.current = setInterval(() => {
      setCount(prev => {
        prevcurent.current = prev        
        return prev - 1
      });
      console.log("start", ref.current);
    }, 1000)
    return () => {
      clearInterval(ref.current, console.log("clear", ref.current))
    }
  }, [run])

  // clear vì luôn luôn lấy được chính xác timerid
  const handleFinish = () => {
    console.log("clear", ref.current);
    clearInterval(ref.current)
  }

  // có thể start nhiều lần vì đã handle clearup
  const handleStart = () => {
    setRun((prev) => {
      return prev + 1
    })
  }

  return (
    <>
      <p>COUNT: {count}</p>
      <hr></hr>
      <p>COUNTPREV: {prevcurent.current}</p>
      <button onClick={handleStart}>handleStart</button>
      <button onClick={handleFinish}>handleFinish</button>

    </>
  )
}
